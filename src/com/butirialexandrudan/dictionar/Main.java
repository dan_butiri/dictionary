package com.butirialexandrudan.dictionar;

public class Main {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Dictionary dictionary = new Dictionary("dictionary.ser");
		MainFrame frame = new MainFrame(dictionary, "dictionary.ser");

		new DictionaryControl(frame.getDictionaryPanel(), dictionary);
	}

}
