package com.butirialexandrudan.dictionar;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -6470090944414208496L;
	private DictionaryPanel dictionaryPanel;
	private Dictionary dictionary;
	private String fileName;

	/**
	 * Create the frame.
	 */
	public MainFrame(Dictionary newDictionary, String newFileName) {
		dictionary = newDictionary;
		fileName = newFileName;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dictionaryPanel = new DictionaryPanel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Dictionar");
		setContentPane(dictionaryPanel);
		setVisible(true);
		setResizable(false);
		pack();

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				dictionary.saveDictionary(fileName);
			}
		});
	}

	public DictionaryPanel getDictionaryPanel() {
		return dictionaryPanel;
	}

}
