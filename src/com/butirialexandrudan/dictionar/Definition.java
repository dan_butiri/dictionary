package com.butirialexandrudan.dictionar;

import java.io.Serializable;

public class Definition implements Serializable{
	private static final long serialVersionUID = -6470090944414208496L;
	private String primaryWord;
	private String tip;
	private String gen;
	private String pl;
	private String articulat;
	private String definitie;
	private String exemplu;
	
	public Definition(){
		primaryWord = "";
		tip = "";
		gen = "";
		pl = "";
		articulat = "";
		definitie = "";
		exemplu = "";
	}

	public String getPrimaryWord() {
		return primaryWord;
	}

	public void setPrimaryWord(String primaryWord) {
		this.primaryWord = primaryWord;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public String getGen() {
		return gen;
	}

	public void setGen(String gen) {
		this.gen = gen;
	}

	public String getPl() {
		return pl;
	}

	public void setPl(String pl) {
		this.pl = pl;
	}

	public String getArticulat() {
		return articulat;
	}

	public void setArticulat(String articulat) {
		this.articulat = articulat;
	}

	public String getDefinitie() {
		return definitie;
	}

	public void setDefinitie(String definitie) {
		this.definitie = definitie;
	}

	public String getExemplu() {
		return exemplu;
	}

	public void setExemplu(String exemplu) {
		this.exemplu = exemplu;
	}
}
