package com.butirialexandrudan.dictionar;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class DictionaryPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private JTextField tfWord;
	private JTextArea taDefinition;
	private JScrollPane spDefinition;
	private DefaultListModel<String> dlmLike;
	private JList<String> lLike;
	private JScrollPane spLike;
	private JButton btnAdd;
	private JButton btnRemove;
	private JButton btnSearch;
	private JButton btnChange;
	private GridBagConstraints gbc;

	/**
	 * Create the panel.
	 */
	public DictionaryPanel() {
		tfWord = new JTextField(" Cuvant" + '\0');
		taDefinition = new JTextArea();
		spDefinition = new JScrollPane(taDefinition);
		dlmLike = new DefaultListModel<String>();
		lLike = new JList<String>(dlmLike);
		spLike = new JScrollPane(lLike);
		btnAdd = new JButton("Adauga cuvant");
		btnRemove = new JButton("Sterge cuvant");
		btnSearch = new JButton("Cauta!");
		btnChange = new JButton("Modifica cuvant");
		gbc = new GridBagConstraints();

		this.setLayout(new GridBagLayout());
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5, 5, 5, 5);

		gbc.ipadx = 500;
		gbc.ipady = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(tfWord, gbc);
		addMessageWord();

		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(btnSearch, gbc);

		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 2;
		gbc.gridy = 0;
		add(btnAdd, gbc);

		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(btnChange, gbc);

		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 2;
		gbc.gridy = 1;
		add(btnRemove, gbc);

		gbc.ipadx = 500;
		gbc.ipady = 200;
		gbc.gridwidth = 1;
		gbc.gridheight = 10;
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(spDefinition, gbc);

		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridwidth = 2;
		gbc.gridheight = 9;
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(spLike, gbc);

		taDefinition.setEditable(false);
	}

	public JButton getBtnAdd() {
		return btnAdd;
	}

	public JTextField getTfWord() {
		return tfWord;
	}

	public JTextArea getTaDefinition() {
		return taDefinition;
	}

	public DefaultListModel<String> getDlmLike() {
		return dlmLike;
	}

	public JList<String> getlLike() {
		return lLike;
	}

	public JButton getBtnRemove() {
		return btnRemove;
	}

	public JButton getBtnSearch() {
		return btnSearch;
	}

	public JButton getBtnChange() {
		return btnChange;
	}

	private void addMessageWord() {
		tfWord.setForeground(Color.LIGHT_GRAY);
		tfWord.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfWord.setCaretPosition(0);
				if (tfWord.getText().compareTo(" Cuvant" + '\0') != 0) {
					tfWord.select(0, tfWord.getText().length());
				}

			}
		});

		tfWord.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfWord.getText().compareTo(" Cuvant" + '\0') == 0) {
					tfWord.setText("");
				}
				tfWord.setForeground(Color.black);
				if (tfWord.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfWord.setText(" Cuvant" + '\0');
					tfWord.setCaretPosition(0);
					tfWord.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfWord.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfWord.getText().compareTo(" Cuvant" + '\0') == 0) {
					tfWord.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfWord.getText().compareTo(" Cuvant" + '\0') == 0) {
					tfWord.setCaretPosition(0);
				}
			}
		});
		tfWord.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfWord.getText().compareTo(" Cuvant" + '\0') == 0) {
					tfWord.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfWord.getText().compareTo(" Cuvant" + '\0') == 0) {
					tfWord.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfWord.getText().compareTo(" Cuvant" + '\0') == 0) {
					tfWord.setCaretPosition(0);
				}
			}
		});
	}
}
