package com.butirialexandrudan.dictionar;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

public class Dictionary implements Serializable {
	private static final long serialVersionUID = -6470090944414208496L;
	private Map<String, Definition> dictionary;

	/**
	 * Creaza obiectul.
	 */
	public Dictionary(String fileName) {
		readAccount(fileName);
	}

	/**
	 * Adauga un nou cuvant cu definitie in dictionar.
	 * 
	 * @param newWord
	 *            cuvantul adaugat.
	 * @param newDefinition
	 *            definitia cuvantului.
	 * @pre newWord != null && newDefinition != null
	 * @post dictionary.get(newWord) == newDefinition
	 */
	public void add(String newWord, Definition newDefinition) {
		assert (newWord != null && newDefinition != null);
		Definition auxDefinition;

		dictionary.put(newWord, newDefinition);

		// Adaugam in dictionar forma de plural.
		auxDefinition = new Definition();
		auxDefinition.setPrimaryWord(newWord);
		auxDefinition.setPl(newDefinition.getPl());
		dictionary.put(newDefinition.getPl(), auxDefinition);

		// Adaugam in dictionar forma articulata.
		auxDefinition = new Definition();
		auxDefinition.setPrimaryWord(newWord);
		auxDefinition.setArticulat(newDefinition.getArticulat());
		dictionary.put(newDefinition.getArticulat(), auxDefinition);

		assert (dictionary.get(newWord) == newDefinition);
	}

	/**
	 * Sterge un cuvant din dictionar.
	 * 
	 * @param removeWord
	 *            cuvantul care se sterge.
	 * @pre removeWord != null
	 * @post dictionary.get(removeWord) == null
	 */
	public void remove(String removeWord) {
		assert (removeWord != null);

		dictionary.remove(dictionary.get(removeWord).getArticulat());
		dictionary.remove(dictionary.get(removeWord).getPl());
		dictionary.remove(removeWord);

		assert (dictionary.get(removeWord) == null);
	}

	// deserialize to Object from given file
	/**
	 * Citeste din fiser lista cu cuvinte. (Obiectul dictionary).
	 * 
	 * @param fileName
	 *            calea catre fiserul de citire.
	 * @pre fileName != null
	 * @post accounts != null
	 */
	@SuppressWarnings("unchecked")
	public void readAccount(String fileName) {
		assert (fileName != null);

		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
					fileName));
			dictionary = (TreeMap<String, Definition>) ois.readObject();

			ois.close();
		} catch (Exception e) {
			dictionary = new TreeMap<String, Definition>();
		}

		assert (dictionary != null);
	}

	// serialize the given object and save it to file
	/**
	 * Scrie in fiser lista cu conturi. (Obiectul accounts).
	 * 
	 * @param fileName
	 *            calea catre fiserul de scriere.
	 * @pre fileName != null
	 * @post
	 */
	public void saveDictionary(String fileName) {
		assert (fileName != null);

		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(fileName));

			oos.writeObject(dictionary);

			oos.close();
		} catch (Exception e) {
		}
	}

	public Map<String, Definition> getDictionary() {
		return dictionary;
	}
}
