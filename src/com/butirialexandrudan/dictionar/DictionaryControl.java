package com.butirialexandrudan.dictionar;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JOptionPane;

public class DictionaryControl {
	DictionaryPanel dictionaryPanel;
	Dictionary dictionary;

	public DictionaryControl(DictionaryPanel newDictionaryPanel,
			Dictionary newDictionary) {
		dictionaryPanel = newDictionaryPanel;
		dictionary = newDictionary;

		dictionaryPanel.getBtnAdd().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				addWord();
			}
		});

		dictionaryPanel.getBtnRemove().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				removeWord();
			}
		});
		
		dictionaryPanel.getBtnChange().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				changeWord();
			}
		});

		dictionaryPanel.getBtnSearch().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				addSearch();
			}
		});

		dictionaryPanel.getTfWord().addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (ke.getKeyChar() == (char) KeyEvent.VK_ENTER) {
					addSearch();
				}
			}
		});

		dictionaryPanel.getTaDefinition().addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (ke.getKeyChar() == (char) KeyEvent.VK_ENTER) {
					addSearch();
				}
			}
		});

		dictionaryPanel.getlLike().addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (ke.getKeyChar() == (char) KeyEvent.VK_ENTER) {
					addSearch();
				}
			}
		});

		dictionaryPanel.getlLike().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				String word = "";

				word = dictionaryPanel.getDlmLike().getElementAt(
						dictionaryPanel.getlLike().getSelectedIndex());
				if (word != null && word.length() != 0) {
					dictionaryPanel.getTfWord().setForeground(Color.BLACK);
					dictionaryPanel.getTfWord().setText(word);
					addHint();
				}
			}
		});

		dictionaryPanel.getTaDefinition().addMouseMotionListener(
				new MouseMotionAdapter() {
					public void mouseDragged(MouseEvent me) {
						addHint();
					}
				});

		dictionaryPanel.getTaDefinition().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				String word = "";

				word = dictionaryPanel.getTaDefinition().getSelectedText();
				if (word != null && word.length() != 0) {
					String[] text = word.split("[ ]+");
					if (text.length != 0) {
						dictionaryPanel.getTfWord().setForeground(Color.BLACK);
						dictionaryPanel.getTfWord().setText(word);
						addHint();
					}
				}
			}
		});
	}

	private void addWord() {
		AddWordPanel addWordPanel = new AddWordPanel();
		Definition auxDefinition = new Definition();
		boolean test = true;
		String errorText = "Completati campurile:\n\n";

		JOptionPane.showMessageDialog(null, addWordPanel, "Cuvant nou!",
				JOptionPane.QUESTION_MESSAGE);

		if (addWordPanel.getTfCuvant().getText().compareTo("") == 0) {
			test = false;
			errorText += "  ->Cuvant\n";
		}
		if (addWordPanel.getTaDefinitie().getText().compareTo("") == 0) {
			test = false;
			errorText += "  ->Definitie\n";
		}
		if (addWordPanel.getTfTip().getText().compareTo("") == 0) {
			test = false;
			errorText += "  ->Tip\n";
		}
		if (addWordPanel.getTfGen().getText().compareTo("") == 0) {
			test = false;
			errorText += "  ->Gen\n";
		}
		if (addWordPanel.getTfPl().getText().compareTo("") == 0) {
			test = false;
			errorText += "  ->Plural\n";
		}
		if (addWordPanel.getTfArticulat().getText().compareTo("") == 0) {
			test = false;
			errorText += "  ->Articulat\n";
		}
		if (addWordPanel.getTaExemple().getText().compareTo("") == 0) {
			test = false;
			errorText += "  ->Exemple\n";
		}

		if (test) {
			auxDefinition.setArticulat(addWordPanel.getTfArticulat().getText()
					.toLowerCase());
			auxDefinition.setDefinitie(addWordPanel.getTaDefinitie().getText()
					.toLowerCase());
			auxDefinition.setExemplu(addWordPanel.getTaExemple().getText()
					.toLowerCase());
			auxDefinition.setGen(addWordPanel.getTfGen().getText()
					.toLowerCase());
			auxDefinition.setPl(addWordPanel.getTfPl().getText().toLowerCase());
			auxDefinition.setPrimaryWord("");
			auxDefinition.setTip(addWordPanel.getTfTip().getText()
					.toLowerCase());

			dictionary.add(addWordPanel.getTfCuvant().getText().toLowerCase(),
					auxDefinition);
		} else {
			errorText += "\nCuvantul nu a fost inregistrat!!!";
			JOptionPane.showMessageDialog(null, errorText, "Avertizare",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void removeWord() {
		String word = dictionaryPanel.getTfWord().getText();
		if (dictionary.getDictionary().get(word) != null) {
			if (dictionary.getDictionary().get(word).getPrimaryWord()
					.compareTo("") == 0) {
				dictionary.remove(word);
			} else {
				JOptionPane.showMessageDialog(null,
						"Cuvantul nu este de baza...\n\n"
								+ "Stergerea nu a reusit!!!", "Avertizare",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private void changeWord(){
		AddWordPanel addWordPanel = new AddWordPanel();
		String word = dictionaryPanel.getTfWord().getText();
		Definition definition = dictionary.getDictionary().get(word);
		if (definition != null) {
			if (definition.getPrimaryWord().compareTo("") == 0) {
				
				addWordPanel.getTfCuvant().setText(word);
				addWordPanel.getTaDefinitie().setText(definition.getDefinitie());
				addWordPanel.getTaExemple().setText(definition.getExemplu());
				addWordPanel.getTfArticulat().setText(definition.getArticulat());
				addWordPanel.getTfGen().setText(definition.getGen());
				addWordPanel.getTfPl().setText(definition.getPl());
				addWordPanel.getTfTip().setText(definition.getTip());
				
				JOptionPane.showMessageDialog(null, addWordPanel, "Modifica cuvat",
						JOptionPane.ERROR_MESSAGE);
				
				
				definition.setArticulat(addWordPanel.getTfArticulat().getText()
						.toLowerCase());
				definition.setDefinitie(addWordPanel.getTaDefinitie().getText()
						.toLowerCase());
				definition.setExemplu(addWordPanel.getTaExemple().getText()
						.toLowerCase());
				definition.setGen(addWordPanel.getTfGen().getText()
						.toLowerCase());
				definition.setPl(addWordPanel.getTfPl().getText().toLowerCase());
				definition.setTip(addWordPanel.getTfTip().getText()
						.toLowerCase());
				
			} else {
				JOptionPane.showMessageDialog(null,
						"Cuvantul nu este de baza...\n\n"
								+ "Stergerea nu a reusit!!!", "Avertizare",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void addSearch() {
		String word = dictionaryPanel.getTfWord().getText();
		Definition definition = null;
		String[] text = word.split("[ ]+");
		dictionaryPanel.getDlmLike().removeAllElements();
		if (text.length != 0) {
			definition = dictionary.getDictionary().get(word.toLowerCase());

			if (definition != null) {
				if (definition.getPrimaryWord().compareTo("") == 0) {
					dictionaryPanel.getTaDefinition().setText("");
					complitDef(word, definition);
				} else {
					if (definition.getPl().compareTo("") != 0) {
						dictionaryPanel.getTaDefinition().setText("");
						dictionaryPanel.getTaDefinition().append(
								word + " (pl.) = ");
						dictionaryPanel.getTaDefinition().append(
								definition.getPrimaryWord());
						dictionaryPanel.getTaDefinition().append("\n");
						complitDef(
								definition.getPrimaryWord(),
								dictionary.getDictionary().get(
										definition.getPrimaryWord()));
					}
					if (definition.getArticulat().compareTo("") != 0) {
						dictionaryPanel.getTaDefinition().setText("");
						dictionaryPanel.getTaDefinition().append(
								word + " (art.) = ");
						dictionaryPanel.getTaDefinition().append(
								definition.getPrimaryWord());
						dictionaryPanel.getTaDefinition().append("\n");
						complitDef(
								definition.getPrimaryWord(),
								dictionary.getDictionary().get(
										definition.getPrimaryWord()));
					}
				}
				// Cautare de subsiruri pentru JList.
				dictionaryPanel.getDlmLike().removeAllElements();
				for (String s : dictionary.getDictionary().keySet()) {
					if (s.toLowerCase().contains(word.toLowerCase())) {
						if (s.compareTo(word) != 0) {
							dictionaryPanel.getDlmLike().addElement(s);
						}
					}
					if (word.toLowerCase().contains(s.toLowerCase())) {
						if (s.compareTo(word) != 0) {
							dictionaryPanel.getDlmLike().addElement(s);
						}
					}
				}
			} else if (word.contains("*") && word.contains("?")) {
				word = word.replace("*", ".*");
				word = word.replace("?", ".?");
				dictionaryPanel.getTaDefinition().setText("| ");
				for (String s : dictionary.getDictionary().keySet()) {
					if (s.matches(word)) {
						dictionaryPanel.getTaDefinition().append(s + " | ");
					}
				}
			} else if (word.contains("*")) {
				word = word.replace("*", ".*");
				dictionaryPanel.getTaDefinition().setText("| ");
				for (String s : dictionary.getDictionary().keySet()) {
					if (s.matches(word)) {
						dictionaryPanel.getTaDefinition().append(s + " | ");
					}
				}
			} else if (word.contains("?")) {
				word = word.replace("?", ".?");
				dictionaryPanel.getTaDefinition().setText("| ");
				for (String s : dictionary.getDictionary().keySet()) {
					if (s.matches(word)) {
						dictionaryPanel.getTaDefinition().append(s + " | ");
					}
				}
			} else {
				dictionaryPanel.getTaDefinition().setText(
						" Cuvantul nu extista in dictionar");
			}
		}
	}

	private void addHint() {
		String word = "";
		String text = "Cuvantul nu exista in dictionar...";
		Definition definition = null;

		word = dictionaryPanel.getTaDefinition().getSelectedText();

		if (word != null && word.length() != 0) {
			String[] auxText = word.split("[ ]+");
			if (auxText.length != 0) {
				definition = dictionary.getDictionary().get(word);
			}
		}

		if (definition != null) {
			if (definition.getPrimaryWord().compareTo("") == 0) {
				text = "";
				text += word + " = ";
				text += definition.getDefinitie();
			} else {
				if (definition.getPl().compareTo("") != 0) {
					text = "";
					text += word + " (pl.) = ";
					text += definition.getPrimaryWord();
				}
				if (definition.getArticulat().compareTo("") != 0) {
					text = "";
					text += word + " (art.) = ";
					text += definition.getPrimaryWord();
				}
			}
		}
		dictionaryPanel.getTaDefinition().setToolTipText(text);
	}

	private void complitDef(String word, Definition definition) {
		dictionaryPanel.getTaDefinition().append(word + " = ");
		dictionaryPanel.getTaDefinition().append(
				"(" + definition.getTip() + ")");
		dictionaryPanel.getTaDefinition().append(" pl.: " + definition.getPl());
		dictionaryPanel.getTaDefinition()
				.append(" gen: " + definition.getGen());
		dictionaryPanel.getTaDefinition().append(
				" def.: " + definition.getDefinitie());
		dictionaryPanel.getTaDefinition().append(
				" art.: " + definition.getArticulat());
		dictionaryPanel.getTaDefinition().append(
				" ex.: " + definition.getExemplu());
	}
}
