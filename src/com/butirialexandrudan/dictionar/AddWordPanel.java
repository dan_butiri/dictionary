package com.butirialexandrudan.dictionar;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class AddWordPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private JTextField tfCuvant;
	private JTextArea taDefinitie;
	private JTextField tfTip;
	private JTextField tfGen;
	private JTextField tfPl;
	private JTextField tfArticulat;
	private JTextArea taExemple;
	private JLabel lblCuvant = new JLabel("Cuvant: ");
	private JLabel lblDefinitie = new JLabel("Definitie: ");
	private JLabel lblTip = new JLabel("Tip: ");
	private JLabel lblGen = new JLabel("Gen: ");
	private JLabel lblPl = new JLabel("Plural: ");
	private JLabel lblArticulat = new JLabel("Articulat: ");
	private JLabel lblExemple = new JLabel("Exemple: ");
	private JScrollPane scDefinitie;
	private JScrollPane scExemple;
	private GridBagConstraints gbc;
	
	/**
	 * Create the panel.
	 */
	public AddWordPanel() {
		tfCuvant = new JTextField();
		taDefinitie = new JTextArea();
		scDefinitie = new JScrollPane(taDefinitie);
		tfTip = new JTextField();
		tfGen = new JTextField();
		tfPl = new JTextField();
		tfArticulat = new JTextField();
		taExemple = new JTextArea();
		scExemple = new JScrollPane(taExemple);
		gbc = new GridBagConstraints();
		
		this.setLayout(new GridBagLayout());
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5, 5, 5, 5);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(lblCuvant, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(tfCuvant, gbc);
		tfCuvant.setColumns(30);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(lblDefinitie, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 5;
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(scDefinitie, gbc);
		taDefinitie.setRows(5);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 6;
		add(lblTip, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 6;
		add(tfTip, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 7;
		add(lblGen, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 7;
		add(tfGen, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 8;
		add(lblPl, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 8;
		add(tfPl, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 9;
		add(lblArticulat, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 9;
		add(tfArticulat, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 10;
		add(lblExemple, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridheight = 5;
		gbc.gridx = 1;
		gbc.gridy = 10;
		add(scExemple, gbc);
		taExemple.setRows(5);
	}

	public JTextField getTfCuvant() {
		return tfCuvant;
	}

	public JTextArea getTaDefinitie() {
		return taDefinitie;
	}

	public JTextField getTfTip() {
		return tfTip;
	}

	public JTextField getTfGen() {
		return tfGen;
	}

	public JTextField getTfPl() {
		return tfPl;
	}

	public JTextField getTfArticulat() {
		return tfArticulat;
	}

	public JTextArea getTaExemple() {
		return taExemple;
	}
}
